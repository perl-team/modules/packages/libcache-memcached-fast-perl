Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Cache::Memcached::Fast
Upstream-Contact: https://github.com/JRaspass/Cache-Memcached-Fast/issues
Source: https://metacpan.org/release/Cache-Memcached-Fast
 https://github.com/JRaspass/Cache-Memcached-Fast

Files: *
Copyright:
  2007-2010  Tomash Brechko <tomash.brechko@gmail.com>
License: LGPL-2.1+ with Perl exception
 When used to build Perl module:
 .
 This library is free software;
 you can redistribute it and/or modify it
 under the same terms as Perl itself,
 either Perl version 5.8.8
 or, at your option,
 any later version of Perl 5 you may have available.
 .
 When used as a standalone library:
 .
 This library is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation;
 either version 2.1 of the License,
 or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

Files: Fast.xs
 lib/Cache/Memcached/Fast.pm
 script/*
 src/gen*.pl
 typemap
Copyright:
  2007-2010  Tomash Brechko <tomash.brechko@gmail.com>
License-Grant:
 This program is free software;
 you can redistribute it and/or modify it
 under the same terms as Perl itself.
License-Grant:
 This library is free software;
 you can redistribute it and/or modify it
 under the same terms as Perl itself,
 either Perl version 5.8.8
 or, at your option,
 any later version of Perl 5 you may have available.
License: Artistic or GPL-1+
Comment:
 Perl is licensed under either the Artistic license
 or the GNU General Public License version 1 or later.

Files: ppport.h
Copyright:
  1999       Kenneth Albanowski
  2004-2013  Marcus Holland-Moritz
  2001       Paul Marquess
License: Artistic or GPL-1+

Files: debian/*
Copyright:
  2010-2011, 2013-2015, 2021-2022  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This packaging is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 3, or (at your option) any later version.
License: GPL-3+
Reference: debian/copyright

License: Artistic
Reference: /usr/share/common-licenses/Artistic

License: GPL-1+
Reference: /usr/share/common-licenses/GPL-1

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3

License: LGPL-2.1+
Reference: /usr/share/common-licenses/LGPL-2.1
